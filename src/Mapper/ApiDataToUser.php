<?php

namespace App\Mapper;

use App\Entity\City;
use App\Entity\EventTeam;
use App\Entity\Province;
use App\Entity\User;
use App\Entity\UserRole;
use App\Entity\UserTeam;
use Doctrine\ORM\EntityManagerInterface;

class ApiDataToUser
{
    private $user;
    private $em;

    public function __construct(User $user, EntityManagerInterface $em)
    {
        $this->user = $user;
        $this->em = $em;
    }

    public function map(array $data, string $targetSite): User
    {
        if ($this->issetAndIsNotEmpty($data, 'username')) {
            $this->user->setUsername($data['username']);
        }

        if ($this->issetAndIsNotEmpty($data, 'lastname')) {
            $this->user->setLastname($data['lastname']);
        }

        if ($this->issetAndIsNotEmpty($data, 'phone')) {
            $this->user->setPhone($data['phone']);
        }

        if ($this->issetAndIsNotEmpty($data, 'city')) {
            $this->user->setCity($data['city']);
        }

        if ($this->issetAndIsNotEmpty($data, 'province')) {
            $province = $this->em->find(Province::class, $data['province']);
            $this->user->setProvince($province);
        }

        if ($this->issetAndIsNotEmpty($data, 'postalCode')) {
            $this->user->setPostalCode($data['postalCode']);
        }

        if ($this->issetAndIsNotEmpty($data, 'street')) {
            $this->user->setStreet($data['street']);
        }

        if ($this->issetAndIsNotEmpty($data, 'homeNumber')) {
            $this->user->setHomeNumber($data['homeNumber']);
        }

        if ($this->issetAndIsNotEmpty($data, 'apartmentNumber')) {
            $this->user->setApartmentNumber($data['apartmentNumber']);
        }

        if ($this->issetAndIsNotEmpty($data, 'apartmentNumber')) {
            $this->user->setApartmentNumber($data['apartmentNumber']);
        }

        if ($this->issetAndIsNotEmpty($data, 'budget')) {
            $this->user->setBudget($data['budget']);
        }

        if ($this->issetAndIsNotEmpty($data, 'facebookId')) {
            $this->user->setFacebookId($data['facebookId']);
        }

        if ($this->issetAndIsNotEmpty($data, 'facebookAccessToken')) {
            $this->user->setFacebookAccessToken($data['facebookAccessToken']);
        }

        if ($this->issetAndIsNotEmpty($data, 'agreeMarketing')) {
            $this->user->setAgreeMarketing($data['agreeMarketing']);
        }

        if ($this->issetAndIsNotEmpty($data, 'agreeRules')) {
            $this->user->setAgreeRules($data['agreeRules']);
        }

        if (isset($data['locked'])) {
            $this->user->isLocked($data['locked']);
        }

        if (isset($data['enabled'])) {
            $this->user->isEnabled($data['enabled']);
        }

        if ($this->issetAndIsNotEmpty($data, 'avatar')) {
            $this->user->setAvatar($data['avatar']);
        }

        if ($this->issetAndIsNotEmpty($data, 'avatarAbsolutePath')) {
            $this->user->setAvatarAbsolutePath($data['avatarAbsolutePath']);
        }

        if ($this->issetAndIsNotEmpty($data, 'createdAt')) {
            $createdAt = new \DateTime($data['createdAt']);
            $this->user->setCreatedAt($createdAt);
        }

        if ($this->issetAndIsNotEmpty($data, 'aboutMe')) {
            $this->user->setAboutMe($data['aboutMe']);
        }

        if ($this->issetAndIsNotEmpty($data, 'instagramUrl')) {
            $this->user->setInstagramUrl($data['instagramUrl']);
        }

        if ($this->issetAndIsNotEmpty($data, 'publishEmailAddress')) {
            $this->user->isPublishEmailAddress($data['publishEmailAddress']);
        }

        if ($this->issetAndIsNotEmpty($data, 'publishPhone')) {
            $this->user->isPublishPhone($data['publishPhone']);
        }

        if ($this->issetAndIsNotEmpty($data, 'publishTimetable')) {
            $this->user->isPublishTimetable($data['publishTimetable']);
        }

        if ($this->issetAndIsNotEmpty($data, 'lastnameCanonical')) {
            $this->user->setLastnameCanonical($data['lastnameCanonical']);
        }

        if ($this->issetAndIsNotEmpty($data, 'isTrusted')) {
            $this->user->setIsTrusted($data['isTrusted']);
        }

        if ($this->issetAndIsNotEmpty($data, 'identityCardNumber')) {
            $this->user->setIdentityCardNumber($data['identityCardNumber']);
        }

        if ($this->issetAndIsNotEmpty($data, 'password')) {
            $this->user->setPassword($data['password']);
        }

        if ($this->issetAndIsNotEmpty($data, 'userRoles')) {
            $userRolesRepo = $this->em->getRepository(UserRole::class);
            $existingUserRoles = $userRolesRepo->findOneBy(['user' => $this->user, 'site' => $targetSite]);

            if ($existingUserRoles) {
                $this->em->remove($existingUserRoles);
                $this->em->flush();
            }

            $userRoles = new UserRole();
            $userRoles->setSite($targetSite);
            $userRoles->setRoles($data['userRoles']);
            $userRoles->setUser($this->user);

            $this->em->persist($userRoles);
            $this->em->flush();
        }


        if ($this->issetAndIsNotEmpty($data, 'teamId')) {
            $eventTeam = $this->em->getRepository(EventTeam::class)->findOneBy(['id' => $data['teamId']]);
            $userTeam = $this->user->getUserTeam();

            if ($userTeam) {
                $userTeam->setEventTeam($eventTeam);
                $this->user->setUserTeam($userTeam);
            } else {
                $userTeam = new UserTeam();
                $userTeam->setUser($this->user);
                $userTeam->setEventTeam($eventTeam);
            }


            $userTeam->setIsCapitanAccepted(isset($data['teamAcceptedByCapitan']) && $data['teamAcceptedByCapitan']);
            $userTeam->setIsAdminAccepted(isset($data['teamAcceptedByAdmin']) && $data['teamAcceptedByAdmin']);


            $this->user->setUserTeam($userTeam);
        } else if($this->user->getUserTeam()) {
            $this->em->remove($this->user->getUserTeam());
            $this->user->setUserTeam(null);
        }

        return $this->user;
    }

    private function issetAndIsNotEmpty(array $values, $key)
    {
        if (!isset($values[$key])) {
            return false;
        }

        return !empty($values[$key]);
    }
}