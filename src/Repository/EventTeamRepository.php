<?php


namespace App\Repository;


use App\Entity\EventTeam;
use Doctrine\ORM\EntityRepository;

class EventTeamRepository extends EntityRepository
{
    public function getTeamByTerm($term)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('et')
            ->from(EventTeam::class, 'et')
            ->where('et.name LIKE :name')
            ->addOrderBy('et.name', 'ASC')
            ->setParameter('name', "%${term}%")
            ->setMaxResults(20);

        return $qb->getQuery()->getResult();
    }

    public function getUserWithoutTeamByTerm($term)
    {
        $term = mb_strtolower($term);

        $conn = $this->getEntityManager()->getConnection();
        $sql = "
        SELECT u.id, u.username, u.lastname, u.email
        FROM `user` u
        WHERE u.id NOT IN (SELECT id_user FROM user_team) AND 
        LOWER(CONCAT(u.username, u.lastname, u.email) LIKE :term OR u.id LIKE :term)
        ORDER BY u.lastname ASC, u.id DESC  
        LIMIT 20
        ";

        $stmt = $conn->prepare($sql);
        $stmt->execute(['term' => "%${term}%"]);

        return $stmt->fetchAll();

    }
}