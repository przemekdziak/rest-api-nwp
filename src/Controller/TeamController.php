<?php


namespace App\Controller;


use App\Entity\Team;
use Doctrine\ORM\QueryBuilder;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\View\View;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpKernel\Exception\HttpException;

class TeamController extends AbstractFOSRestController
{
    /**
     * @Rest\Get("/api/teams/search-team")
     * @param Request $request
     * @return View
     */
    public function getTeamByTerm(Request $request): View
    {
        $term = $request->query->get('term');

        if (mb_strlen($term) < 3) {
            throw new HttpException(400, 'Term has to be min. 3 charasters in length');
        }

        $result = $this
            ->getDoctrine()
            ->getRepository(EventTeam::class)
            ->getTeamByTerm($term);

        $list = [];
        /**
         * @var $team EventTeam
         */
        foreach ($result as $team) {
            $list[] = [
                'id' => $team->getId(),
                'name' => $team->getName()
            ];
        }

        return $this->view($list);
    }

    /**
     * @Rest\Get("/api/teams/search-member")
     * @param Request $request
     * @return View
     */
    public function searchMembers(Request $request) {
        $term = $request->query->get('term');

        if (mb_strlen($term) < 3) {
            throw new HttpException(400, 'Term has to be min. 3 charasters in length');
        }

        $result = $this
            ->getDoctrine()
            ->getRepository(EventTeam::class)
            ->getUserWithoutTeamByTerm($term);

        return $this->view($result);
    }

    /**
     * @Rest\Get("/api/teams/user/{id}")
     * @param int $id
     * @return View
     */
    public function getUserTeam(int $id)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(User::class);
        $user = $repo->findOneBy(['id' => $id]);

        $data = [];
        if ($user->getUserTeam()) {
            $data = [
                'id' => $user->getUserTeam()->getEventTeam()->getId(),
                'name' => $user->getUserTeam()->getEventTeam()->getName(),
                'teamAcceptedByCapitan' => $user->getUserTeam()->isCapitanAccepted(),
                'teamAcceptedByAdmin' => $user->getUserTeam()->isAdminAccepted(),
            ];
        }

        return $this->view($data);
    }

    /**
     * @Rest\Get("/api/teams")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return View
     */
    public function getTeams(Request $request, PaginatorInterface $paginator): View
    {
        return $this->view(['test' => 1]);
        $em = $this->getDoctrine()->getManager();
        /**
         * @var $query QueryBuilder
         */
        $query = $em
            ->createQueryBuilder()
            ->select('et')
            ->from(EventTeam::class, 'et')
            ->orderBy('et.name', 'ASC');

        $term = $request->query->get('term');
        if ($term) {
            $query->where('et.name LIKE :term');
            $query->setParameter('term', '%' . $term . '%');
        }

        $pagination = $paginator->paginate($query, $request->get('page', 1), 20);

        $view = $this->view($pagination);
        $view->getContext()->setGroups(['teams_list', 'pagination']);

        return $view;
    }

    /**
     * @Rest\Get("/api/teams/members/{id}", requirements={"id"="\d+"})
     * @param int $id
     * @return View
     */
    public function getTeamMembers(int $id)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(UserTeam::class);
        $eventTeam = $em->find(EventTeam::class, $id);

        $userTeams = $repo->findBy(['eventTeam' => $eventTeam]);

        $view = $this->view($userTeams);
        $result = [];
        /**
         * @var $team UserTeam
         */
        foreach ($userTeams as $team) {
            $result[] = [
                'id' => $team->getUser()->getId(),
                'fullUsername' => $team->getUser()->getFullUsername(),
                'isAdminAccepted' => $team->isAdminAccepted(),
                'isCapitanAccepted' => $team->isCapitanAccepted(),
                'isModerator' => $team->isModerator(),
            ];
        }

        return $this->view($result);
    }

    /**
     * @Rest\Get("/api/teams/{id}", requirements={"id"="\d+"})
     * @param int $id
     * @return View
     */
    public function getTeam(int $id)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(Team::class);

        $team = $repo->findOneBy(['id' => $id]);

        return $this->view($team);
    }

    /**
     * @Rest\Get("/api/teams/by-email")
     * @param Request $request
     * @return View
     */
    public function getTeamByEmail(Request $request) {
        $email = $request->query->get('email');

        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(Team::class);

        $team = $repo->findOneBy(['ownerEmail' => $email]);

        return $this->view($team ?: []);
    }

    /**
     * @Rest\Put("/api/teams", requirements={"id"="\d+"})
     * @param Request $request
     * @return View
     */
    public function createTeam(Request $request)
    {

        $teamName = $request->request->get('team_name');
        $teamCity = $request->request->get('team_city');
        $email = $request->request->get('team_email_address');
        $phone = $request->request->get('team_phone');
        $about = $request->request->get('team_about');
        $instagramUrl = $request->request->get('team_instagram_url');
        $facebookUrl = $request->request->get('team_facebook_url');
        $youtubeUrl = $request->request->get('team_youtube_url');
        $siteUrl = $request->request->get('team_www_url');
        $logo = $request->request->get('logo_filename');
        $locale = $request->request->get('locale');

        $team = new Team();
        $team->setTeamName($teamName);
        $team->setEmail($email);
        $team->setInstagramUrl($instagramUrl);
        $team->setFacebookUrl($facebookUrl);
        $team->setYoutubeUrl($youtubeUrl);
        $team->setSiteUrl($siteUrl);
        $team->setPhoneNumber($phone);
        $team->setCountryId(11);
        $team->setLogoFilename($logo);
        $team->translate($locale)->setCity($teamCity);
        $team->translate($locale)->setAbout($about);
//
//        $em = $this->getDoctrine()->getManager();
//        $em->persist($team);
//
//        $team->mergeNewTranslations();
//
//        $em->flush();

        return $this->view([
            'status' => 0,
            'id' => 24,//$team->getId()
        ]);
    }



    /**
     * @Rest\Post("/api/teams/{id}", requirements={"id"="\d+"})
     * @param Request $request
     * @param int $id
     * @return View
     */
    public function updateTeam(Request $request, int $id)
    {
        $team = $this->getDoctrine()->getRepository(Team::class)->find($id);
        $locale = $request->request->get('locale');

        if($request->request->has('team_city')) {
            $team->translate($locale)->setCity($request->request->get('team_city'));
        }

        if($request->request->has('team_about')) {
            $team->translate($locale)->setAbout($request->request->get('team_about'));
        }

        if($request->request->has('logo_filename')) {
            $team->setLogoFilename($request->request->get('logo_filename'));
        }

        if($request->request->has('team_name')) {
            $team->setTeamName($request->request->get('team_name'));
        }

        if($request->request->has('team_email_address')) {
            $team->setLogoFilename($request->request->get('team_email_address'));
        }

        if($request->request->has('team_instagram_url')) {
            $team->setInstagramUrl($request->request->get('team_instagram_url'));
        }

        if($request->request->has('team_facebook_url')) {
            $team->setFacebookUrl($request->request->get('team_facebook_url'));
        }

        if($request->request->has('team_youtube_url')) {
            $team->setYoutubeUrl($request->request->get('team_youtube_url'));
        }

        if($request->request->has('team_www_url')) {
            $team->setSiteUrl($request->request->get('team_www_url'));
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($team);
        $em->flush();

//        $team = $this->getTeamById($id);
//        $team->setName($request->request->get('name'));
//        $userRoles = $request->request->get('userRoles');
//
//        $em = $this->getDoctrine()->getManager();
//        $repo = $em->getRepository(UserTeam::class);
//        foreach ($userRoles as $idUser => $role) {
//            $userTeam = $repo->findOneBy(['eventTeam' => $team, 'idUser' => $idUser]);
//            if ($role === UserTeam::ROLE_MODERATOR) {
//                $userTeam->setIsModerator(true);
//                $em->persist($userTeam);
//            } else if ($role === 'remove') {
//                $em->remove($userTeam);
//            } else {
//                $userTeam->setIsModerator(false);
//                $em->persist($userTeam);
//            }
//
//            $em->flush();
//        }

        return $this->view(['status' => 0]);
    }

    /**
     * @Rest\Delete("/api/teams/{id}", requirements={"id"="\d+"})
     * @param Request $request
     * @param int $id
     * @return View
     */
    public function removeTeam(Request $request, int $id)
    {
        $team = $this->getTeamById($id);

        $em = $this->getDoctrine()->getManager();
        $em->remove($team);
        $em->flush();

        return $this->view(['status' => 0]);
    }

    private function getTeamById(int $id)
    {
        $em = $this->getDoctrine()->getManager();
        return $em->getRepository(EventTeam::class)->findOneBy(['id' => $id]);
    }
}