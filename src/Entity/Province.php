<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * City
 *
 * @ORM\Table(name="province")
 * @ORM\Entity
 */
class Province
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="province", type="string", length=255, nullable=false)
     */
    private $province;

    /**
     * @var string
     * @Gedmo\Slug(fields={"province"}, updatable=false, separator="-", unique=true)
     * @ORM\Column(name="slug", type="string", length=255, nullable=false)
     */
    private $slug;

    /**
     * @var float
     *
     * @ORM\Column(name="map_coords_x", type="float", nullable=true)
     */
    private $mapCoordsX;

    /**
     * @var float
     *
     * @ORM\Column(name="map_coords_y", type="float", nullable=true)
     */
    private $mapCoordsY;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getProvince(): string
    {
        return $this->province;
    }

    /**
     * @param string $province
     */
    public function setProvince(string $province)
    {
        $this->province = $province;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return float
     */
    public function getMapCoordsX(): ?float
    {
        return $this->mapCoordsX;
    }

    /**
     * @param float $mapCoordsX
     */
    public function setMapCoordsX(?float $mapCoordsX)
    {
        $this->mapCoordsX = $mapCoordsX;
    }

    /**
     * @return float
     */
    public function getMapCoordsY(): ?float
    {
        return $this->mapCoordsY;
    }

    /**
     * @param float $mapCoordsY
     */
    public function setMapCoordsY(?float $mapCoordsY)
    {
        $this->mapCoordsY = $mapCoordsY;
    }

    public function jsonSerialize() {
        return $this->id;
    }

}
