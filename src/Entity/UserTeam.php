<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TranslatableInterface;
use Knp\DoctrineBehaviors\Model\Translatable\TranslatableTrait;

/**
 * Team
 *
 * @ORM\Table(name="user_team")
 * @ORM\Entity
 */
class UserTeam implements TranslatableInterface
{
    use TranslatableTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="team_name", type="string", length=255, nullable=false)
     */
    private $teamName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="phone_number", type="string", length=32, nullable=true)
     */
    private $phoneNumber;

    /**
     * @var int
     *
     * @ORM\Column(name="country_id", type="integer", nullable=false)
     */
    private $countryId;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", nullable=false, length=128)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="instagram_url", type="string", length=512, nullable=true)
     */
    private $instagramUrl;

    /**
     * @var string|null
     *
     * @ORM\Column(name="youtube_url", type="string", length=512, nullable=true)
     */
    private $youtubeUrl;

    /**
     * @var string|null
     *
     * @ORM\Column(name="facebook_url", type="string", length=512, nullable=true)
     */
    private $facebookUrl;

    /**
     * @var string|null
     *
     * @ORM\Column(name="site_url", type="string", length=512, nullable=true)
     */
    private $siteUrl;

    /**
     * @var string|null
     *
     * @ORM\Column(name="logo_filename", type="string", length=256, nullable=true)
     */
    private $logoFilename;

    /**
     * @var string
     *
     * @ORM\Column(name="owner_email", type="string", length=128, nullable=false)
     */
    private $ownerEmail;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTeamName(): string
    {
        return $this->teamName;
    }

    /**
     * @param string $teamName
     */
    public function setTeamName(string $teamName): void
    {
        $this->teamName = $teamName;
    }

    /**
     * @return string|null
     */
    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    /**
     * @param string|null $phoneNumber
     */
    public function setPhoneNumber(?string $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return int
     */
    public function getCountryId(): int
    {
        return $this->countryId;
    }

    /**
     * @param int $countryId
     */
    public function setCountryId(int $countryId): void
    {
        $this->countryId = $countryId;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string|null
     */
    public function getInstagramUrl(): ?string
    {
        return $this->instagramUrl;
    }


    /**
     * @param string|null $instagramUrl
     */
    public function setInstagramUrl(?string $instagramUrl): void
    {
        $this->instagramUrl = $instagramUrl;
    }

    /**
     * @return string|null
     */
    public function getYoutubeUrl(): ?string
    {
        return $this->youtubeUrl;
    }

    /**
     * @param string|null $youtubeUrl
     */
    public function setYoutubeUrl(?string $youtubeUrl): void
    {
        $this->youtubeUrl = $youtubeUrl;
    }

    /**
     * @return string|null
     */
    public function getFacebookUrl(): ?string
    {
        return $this->facebookUrl;
    }

    /**
     * @param string|null $facebookUrl
     */
    public function setFacebookUrl(?string $facebookUrl): void
    {
        $this->facebookUrl = $facebookUrl;
    }

    /**
     * @return string|null
     */
    public function getSiteUrl(): ?string
    {
        return $this->siteUrl;
    }

    /**
     * @param string|null $siteUrl
     */
    public function setSiteUrl(?string $siteUrl): void
    {
        $this->siteUrl = $siteUrl;
    }

    /**
     * @return string|null
     */
    public function getLogoFilename(): ?string
    {
        return $this->logoFilename;
    }

    /**
     * @param string|null $logoFilename
     */
    public function setLogoFilename(?string $logoFilename = null): void
    {
        $this->logoFilename = $logoFilename;
    }



}
